package Sample;

public final class SingletonFlower {
	private static SingletonFlower theInstanse;
	private SingletonFlower(){}
	public static SingletonFlower getInstanse(){
		if(theInstanse==null){
			theInstanse = new SingletonFlower();
		}
		return theInstanse;
	}


}
